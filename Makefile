.RECIPEPREFIX+= 

SCOPES_INSTALL_DIRECTORY=/usr/local/lib/scopes/

.PHONY: check install test

check: init.sc
    scopes init.sc

test: init.sc testing/test.sc
    scopes testing/test.sc

install: init.sc
    rm $(SCOPES_INSTALL_DIRECTORY)/LinkedArray.sc
    ln -s $(shell pwd)/init.sc $(SCOPES_INSTALL_DIRECTORY)/LinkedArray.sc

