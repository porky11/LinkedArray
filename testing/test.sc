using import ..init

let TESTSIZE = (1:usize << 16:usize)
let fullrange = (range (unconst TESTSIZE))

fn autodelete (x)
    fn (...)
        delete x
        ...

fn test-array-of-array (x y)
    print
        dump "test-array-of-array" x y
    # array of array
    let i32Array = (LinkedArray i32 y)
    let i32ArrayArray = (LinkedArray i32Array x)
    let a = (local i32ArrayArray)
    # will also delete the nested array
    defer (autodelete a)
    for x in (range 16)
        let b =
            'emplace-append a
        assert ((countof b) == 0)
        for y in (range 16)
            'append b (x * 16 + y)
    print a
    for x b in (enumerate a)
        print b
        for y n in (enumerate b)
            assert ((x * 16 + y) == (255 - n))

#
    test-array-of-array (x = 16) (y = 16)
    test-array-of-array (y = 16)
    test-array-of-array (x = 16)
test-array-of-array;

#do
    # mutable array with fixed upper capacity
    let i32Arrayx65536 = (LinkedArray i32 TESTSIZE)
    let a = (local i32Arrayx65536)
    defer (autodelete a)
    for i in fullrange
        assert ((countof a) == i)
        'append a (i32 i)
    for i in fullrange
        assert ((a @ i) == (i32 i))
    # generator support
    for i k in (enumerate a)
        assert ((a @ i) == i)

#do
    # mutable array with dynamic capacity
    let i32Array = (LinkedArray i32)
    let a = (local i32Array (capacity = 12))
    defer (autodelete a)
    for i in fullrange
        assert ((countof a) == i)
        'append a (i32 i)
    #for i in fullrange
        assert ((a @ i) == (i32 i))
    # generator support
    #for i k in (enumerate a)
        assert ((a @ i) == i)

fn test-remove-array-elements ()
    print
        dump "test-remove-array-elements"
    let i32Array = (LinkedArray i32)
    let i32ArrayArray = (LinkedArray i32Array)
    let a = (local i32ArrayArray)
    
    defer (autodelete a)
    for x in (range 4)
        let b =
            'emplace-append a
        assert ((countof b) == 0)
        assert (b._first-index == 0:usize)
        for y in (range 0x100)
            let new =
                'append b y
        assert ((countof b) == 0x100)
    for b x in a
        let div = (x + 2)
        for n y in b
            if (((usize n) % div) != 0)
                'remove-at b y
        let expected =
            (0x100:usize // (usize div)) + (? ((0x100 % div) == 0) 0:usize 1:usize)
        print "div:" div "count:" (countof b) "expected:" expected
        assert ((countof b) == expected)
        if ((countof b) <= 0x40)
            'remove-at a x
    assert ((countof a) == 2)

test-remove-array-elements;

#fn test-sort-array (T)
    dump "testing sorting" T
    # sorting a fixed mutable array
    let a = (local T)
    defer (autodelete a)
    for k in (va-each 3 1 9 5 0 7 12 3 99 -20)
        'append a k
    for i k in (enumerate (va-each 3 1 9 5 0 7 12 3 99 -20))
        assert ((a @ i) == k)
    'sort a
    for i k in (enumerate (va-each -20 0 1 3 3 5 7 9 12 99))
        assert ((a @ i) == k)
    # custom sorting key
    'sort a
        fn (x)
            - x
    for i k in (enumerate (va-each 99 12 9 7 5 3 3 1 0 -20))
        assert ((a @ i) == k)

#test-sort-array (LinkedArray i32 32)
#test-sort-array (LinkedArray i32)

#dump "sorting a bunch of values"

#do
    let a = (local (LinkedArray string))
    defer (autodelete a)
    let word = "yes"
    for k in (va-each word "this" "is" "dog" "")
        'append a k
    assert ((countof a) == 5)
    for i k in (enumerate (va-each "yes" "this" "is" "dog" ""))
        assert ((a @ i) == k)
    'sort a
    for i k in (enumerate (va-each "" "dog" "is" "this" "yes"))
        assert ((a @ i) == k)

#dump "sorting big array"
#print "big sort"

#fn test-sort ()
    let a = (local (LinkedArray i32))
    let N = 1000000
    for i in (range N)
        'append a
            if ((i % 2) == 0)
                i
            else
                N - i
    print "sorting big array..."
    'sort a
    print "done."
    # verify the array is sorted
    let x =
        local 'copy (deref (a @ 0))
    for k in a
        let x1 = (deref k)
        assert (x1 >= x)
        x = x1

#test-sort;

