using import Array
using import utils.va

let LinkedArray LinkedArrayElement =
    typename "LinkedArray"
        super = CStruct
    typename "LinkedArrayElement"
        super = CStruct

typefn LinkedArrayElement '__typecall (cls element-type)
    struct
        .. "<LinkedArrayElement " 
            .. "" "" (string-repr element-type)
            ">"
        _next-index : usize
        _prev-index : usize
        _deleted? : bool
        _data : element-type
        set-typename-super! this-struct LinkedArrayElement
        set-type-symbol! this-struct 'ElementType element-type
        method& 'deleted? (self)
            deref self._deleted?

typefn LinkedArray '__typecall (cls element-type)
    let array-element-type =
        LinkedArrayElement element-type
    struct
        .. "<LinkedArray " 
            .. "" "" (va-map string-repr element-type)
            ">"
        _count* : usize
        _first-index : usize
        _free-index : usize
        _data : (Array array-element-type) #TODO: more types
        set-typename-super! this-struct LinkedArray
        set-type-symbol! this-struct 'ElementType element-type
        

typefn& LinkedArray '__new (self)
    self._count* = 0:usize
    self._first-index = 0:usize
    self._free-index = 0:usize
    construct self._data

typefn& LinkedArrayElement '__new (self _first-index ...)
    self._next-index = _first-index
    self._prev-index = 0:usize
    self._deleted? = false
    construct self._data ...

typefn& LinkedArray '__copy (self other)
    self._count* = other._count*
    self._first-index = other._first-index
    self._free-index = other._free-index
    copy-construct self._data other._data

typefn& LinkedArrayElement '__copy (self other)
    self._next-index = other._next-index
    self._prev-index = other._prev-index
    # TODO: maybe disallow copy-construct of deleted? objects
    self._deleted? = other._deleted?
    copy-construct self._data other._data

typefn& LinkedArray '__move (self other)
    self._count* = other._count*
    self._first-index = other._first-index
    self._free-index = other._free-index
    move-construct self._data other._data

typefn& LinkedArrayElement '__move (self other)
    self._next-index = other._next-index
    self._prev-index = other._prev-index
    self._deleted? = other._deleted?
    move-construct self._data other._data

typefn& LinkedArray '__delete (self other)
    destruct self._data

typefn& LinkedArrayElement '__delete (self other)
    if (deref self._deleted?)
        return;
    destruct self._data


typefn& LinkedArray 'append (self value)
    self._count* += 1:usize
    let T = (typeof& self)
    let value-type =
        LinkedArrayElement T.ElementType
    let first-index =
        deref self._first-index
    let element-value =
        local value-type first-index
    if ((typeof value) < ref)
        # TODO: check, if move construct is applicable here
        move-construct element-value._data value
    else
        element-value._data = value
    let result new-first-index =
        if (self._free-index == 0)
            let result =
                'append self._data element-value
            _ result (countof self._data)
        else
            let recycle-data =
                self._data @ (self._free-index - 1:usize)
            let free-index =
                deref self._free-index
            self._free-index = recycle-data._prev-index
            copy-construct recycle-data element-value
            _ recycle-data free-index
    self._first-index = new-first-index
    if (first-index != 0)
        let first-data =
            self._data @ (first-index - 1:usize)
        first-data._prev-index = new-first-index
    result

typefn& LinkedArray 'emplace-append (self ...)
    self._count* += 1:usize
    let T = (typeof& self)
    let first-index =
        deref self._first-index
    if (first-index != 0)
        let first-data =
            self._data @ (first-index - 1:usize)
    let result new-first-index =
        if (self._free-index == 0)
            let result =
                'emplace-append self._data first-index ...
            _ result (countof self._data)
        else
            let recycle-data =
                self._data @ (self._free-index - 1:usize)
            let free-index =
                deref self._free-index
            self._free-index = recycle-data._prev-index
            construct recycle-data first-index ...
            _ recycle-data free-index
    self._first-index = new-first-index
    if (first-index != 0)
        let first-data =
            self._data @ (first-index - 1:usize)
        first-data._prev-index = new-first-index

    result

fn remove-at (self index delete-data)
    self._count* -= 1:usize

    destruct delete-data._data
    
    let prev-index next-index =
        deref delete-data._prev-index
        deref delete-data._next-index
    if (next-index != 0)
        let next = (self._data @ (next-index - 1:usize))
        next._prev-index = prev-index
    if (prev-index != 0)
        let prev = (self._data @ (prev-index - 1:usize))
        prev._next-index = next-index
    else
        self._first-index = next-index

    delete-data._deleted? = true
    delete-data._prev-index =
        deref self._free-index

    self._free-index = index

typefn& LinkedArray 'remove-at (self index)
    let index =
        usize index
    let delete-data =
        self._data @ index
    if ('deleted? delete-data)
        error! "trying to delete already deleted data"

    remove-at self (index + 1:usize) delete-data

#typefn& LinkedArray 'remove (self delete-data)
    if ('deleted? delete-data)
        error! "trying to delete already deleted data"
    let prev-index =
        delete-data._prev-index
    let index =
        if (prev-index == 0)
            deref self._free-index
        else
            let prev = (self._data @ (prev-index - 1:usize))
            deref self._next-index

    remove-at self index delete-data


typefn& LinkedArray 'clear (self)
    self._count* = 0:usize
    self._first-index = 0:usize
    self._free-index = 0:usize
    'clear self._data

typefn& LinkedArray '__countof (self)
    deref self._count*

typefn& LinkedArrayElement '__deref (self)
    if (deref self._deleted?)
        error! "trying to access deleted array element"
    self._data

let getattr =
    type@& LinkedArrayElement '__getattr

typefn& LinkedArrayElement '__getattr (self name)
    let result... =
        getattr self name
    if (va-empty? result...)
        forward-getattr (deref self) name
    else
        result...

typefn& LinkedArrayElement '__repr (self)
    '__repr self._data

typefn& LinkedArrayElement '__typeattr (T symbol)
    let method =
        forward-typeattr (ref (pointer T.ElementType 'mutable)) symbol
    if (none? method)
        _ none false
    else
        _
            fn (self ...)
                method (deref self) ...
            true

typefn& LinkedArray '__@ (self index)
    let result =
        self._data @ index
    _ result result._deleted?

typefn& LinkedArray '__as (self T)
    if (T == Generator)
        Generator
            label (fret fdone i)
                if (i == 0)
                    fdone;
                else
                    let next =
                        self._data @ (i - 1:usize)
                    fret next._next-index next._data (i - 1:usize)
            self._first-index
    else
        return;

do
    let LinkedArray
    locals;
